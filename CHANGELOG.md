# Changelog

## 1.0.1

- Reduce verbosity in steps `publishArtifact` and `fetchArtifact` by executing Maven in batch mode. 
This improves overall readability of the job console logs.

## 1.0.0

Initial version that contains the following steps:
- `checkoutProject`
- `createDockerImage`
- `deployToECS`
- `fetchArtifact`
- `promoteDeployment`
- `publishArtifact`
- `pushDockerImageToRegistry`
- `sendBuildNotification`
- `triggerDeployment`
