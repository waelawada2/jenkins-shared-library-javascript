# Jenkins Shared Library for JavaScript

This library helps with Jenkins CI/CD of JavaScript applications in Sotheby's ECS environment.

## Prerequisites

In order to build and deploy JavaScript applications with Jenkins multibranch pipelines using this Jenkins library, the project must adhere the following conventions:

- Must have a `package.json` file in the project root. The file must contain the following fields:
  - `name`: The name of the application.
  - `version`: The version of the application following [semantic versioning](https://semver.org/). Additionally, development versions must contain the `-SNAPSHOT` suffix (e.g. `1.0.0-SNAPSHOT`).
  - `groupId`: The group ID of the artifact that is used when publishing the artifact in the artifact repository. Should follow the reverse domain naming notation and start with `com.sothebys` (e.g. `com.sothebys.project`).
- Must contain a `Dockerfile` in the project root.
- Must contain ECS task definition files named `taskdef.ENVIRONMENT.ecs.json` in the project root, where `ENVIRONMENT` is the name of the target deployment environment (e.g. `dev` or `prd`).

## Usage

The library can be included in `Jenkinsfile`s as follows:

```
@Library('sothebys-jenkins-shared-library-javascript@1.x') _
``` 

## Versioning

The library uses [semantic versioning](https://semver.org/) for managing released versions.

Major versions are managed on separate branches.
The naming scheme is `MAJOR.x`, e.g. `1.x`.

Minor and patch versions are implemented with tags.
The naming scheme is `MAJOR.MINOR.PATCH`, e.g. `1.0.0`.

Clients can include library using the major version branch: and therefore will receive all minor version and patch version improvements without applying changes to the pipelines:

```
@Library('sothebys-jenkins-shared-library-javascript@1.x') _
``` 

Alternatively, clients can included a specific version by targeting the tag which can improve stability, but requires changes to the pipelines for updates.

```
@Library('sothebys-jenkins-shared-library-javascript@1.0.0') _
``` 
