package com.sothebys.jenkins.js

/**
 * Contains global constants about deployment environments.
 *
 * @author Gregor Zurowski
 */
class Constants {

  /**
   * The suffix used for snapshot versions.
   */
  static final def SNAPSHOT_SUFFIX = '-SNAPSHOT'

  /**
   * The default dist directory (with the build results).
   */
  static final def DEFAULT_DIST_DIR = 'dist'

  /**
   * The default dist archive name (of the archived build results).
   */
  static final def DEFAULT_DIST_ARCHIVE_NAME = 'dist.zip'

  /**
   * The base URI of Sotheby's ECR registry.
   */
  static final def ECR_BASE_URI = '368978185270.dkr.ecr.us-east-1.amazonaws.com'

  /**
   * AWS credential ID by environment
   */
  static final def AWS_CREDENTIALS = [
      dev: 'aws-dev-credentials',
      qa : 'aws-prd-credentials',
      stg: 'aws-prd-credentials',
      prd: 'aws-prd-credentials',
  ]

  /**
   * Ordered list of default deployment environments
   */
  static final def DEPLOYMENT_ENVIRONMENTS = ['dev', 'qa', 'stg', 'prd']

  /**
   * The settings ID of the default Maven settings.
   */
  static final def DEFAULT_MAVEN_SETTINGS_ID = 'maven-settings-global'

  /**
   * The repository ID of Sotheby's artifact repository for releases.
   */
  static final def REPO_ID_SOTHEBYS_RELEASES = 'sothebys-maven-releases'

  /**
   * The repository ID of Sotheby's artifact repository for snapshots.
   */
  static final def REPO_ID_SOTHEBYS_SNAPSHOTS = 'sothebys-maven-snapshots'

  /**
   * The base URL of Sotheby's Nexus server.
   */
  static final def REPO_URL_SOTHEBYS = 'http://nexus.aws.sothebys.com/repository/'

}