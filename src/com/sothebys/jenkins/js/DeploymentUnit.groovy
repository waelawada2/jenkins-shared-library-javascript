package com.sothebys.jenkins.js

import static com.sothebys.jenkins.js.Constants.*

/**
 * This class represents a deployment unit.
 *
 * @author Gregor Zurowski
 */
class DeploymentUnit implements Serializable {

  def groupId
  def artifactId
  def version
  def buildNumber

  /**
   * All arguments constructor.
   *
   * @param groupId The group ID.
   * @param artifactId The artifact ID.
   * @param version The version.
   * @param buildNumber The current build number.
   */
  DeploymentUnit(groupId, artifactId, version, buildNumber) {
    this.groupId = groupId
    this.artifactId = artifactId
    this.version = version
    this.buildNumber = buildNumber
  }

  /**
   * Returns the Docker tag for this {@link DeploymentUnit} which includes the version number
   * and the build number by default.
   *
   * @return The Docker tag.
   */
  def getDockerTag() {
    return getDockerTag(this.version, this.buildNumber)
  }

  /**
   * Returns the Docker tag for the given {@code version} and {@code buildNumber}.
   *
   * @param version The artifact version.
   * @param buildNumber The Jenkins job build number.
   * @return The Docker tag.
   */
  static def getDockerTag(String version, Integer buildNumber) {
    return "${version}_build-${buildNumber}"
  }

  /**
   * Checks whether this {@link DeploymentUnit} is a snapshot version.
   *
   * @return {@code true} for snapshot versions, otherwise {@code false}.
   */
  def isSnapshot() {
    return isSnapshot(this.version)
  }

  /**
   * Checks whether the given {@code version} is a snapshot version.
   *
   * @param version The version number.
   * @return {@code true} for snapshot versions, otherwise {@code false}.
   */
  static def isSnapshot(String version) {
    return version.endsWith(SNAPSHOT_SUFFIX)
  }

  /**
   * Creates a version for the given for this {@link DeploymentUnit}.
   * This method encodes the branch name in the version, if required.
   * This is required for all snapshot version that are managed on other branches than {@code develop}.
   * <br/>
   * For example: {@code 1.0.0-feature-new-SNAPSHOT}, where {@code 1.0.0} is the version number,
   * {@code feature-name} the branch name, and {@code -SNAPSHOT} the snapshot suffix.
   */
  def createRepositoryVersionFor(String branchName) {
    return createRepositoryVersionFor(this.version, branchName)
  }

  /**
   * Creates a version for the given {@code version} and {@code branchName}.
   * This method encodes the branch name in the version, if required.
   * This is required for all snapshot version that are managed on other branches than {@code develop}.
   * <br/>
   * For example: {@code 1.0.0-feature-new-SNAPSHOT}, where {@code 1.0.0} is the version number,
   * {@code feature-name} the branch name, and {@code -SNAPSHOT} the snapshot suffix.
   *
   * @param branchName The current branch name.
   * @return The version including the branch name if encoding is required, otherwise the version
   *    as passed into the method.
   */
  static def createRepositoryVersionFor(String version, String branchName) {
    return isSnapshot(version) && branchName != 'develop' ?
        createVersionWithBranchName(version, branchName) : version
  }

  /**
   * Creates a version that encodes the branch name.
   * <br/>
   * For example: {@code 1.0.0-feature-new-SNAPSHOT}, where {@code 1.0.0} is the version number,
   * {@code feature-name} the branch name, and {@code -SNAPSHOT} the snapshot suffix.
   *
   * @param branchName The current branch.
   * @return The version including the branch name.
   */
  def createVersionWithBranchName(String branchName) {
    return createVersionWithBranchName(this.version, branchName)
  }

  /**
   * Creates a version that encodes the branch name.
   * <br/>
   * @param version The version number of the artifact.
   * @param branchName The current branch name.
   * @return The version including the branch name.
   */
  static def createVersionWithBranchName(String version, String branchName) {
    // replace slash to avoid creation of sub-directories in Nexus
    branchName = branchName.replaceAll('/', '-')

    return isSnapshot(version) ?
        version.replaceAll(SNAPSHOT_SUFFIX, '') + '-' + branchName + SNAPSHOT_SUFFIX :
        version + '-' + branchName
  }

}