package com.sothebys.jenkins.js

import groovy.json.JsonOutput
import groovy.json.JsonSlurper

/*
 * Contains utility methods for simplifying build and deployment actions.
 *
 * @author Gregor Zurowski
 */

/**
 * Factory method for creating DeploymentUnits by retrieving metadata from the
 * project's package.json file.
 *
 * @return A DeploymentUnit instance.
 */
def createDeploymentUnit() {
  def json = readFile('package.json')
  def result = new JsonSlurper().parseText(json)
  return new DeploymentUnit(result.get('groupId'), result.get('name'), result.get('version'), currentBuild.number)
}

/**
 * Checks whether the current workspace contains a snapshot version.
 *
 * @return {@code true} for snapshot versions, otherwise {@code false}.
 */
def isSnapshot() {
  return createDeploymentUnit().isSnapshot()
}

/**
 * Checks whether the given branch is a deployable branch.
 * Deployable branches are the develop, master and release branches as per Gitflow.
 *
 * @param branchName The branch name.
 * @return {@code true} for deployable branches, otherwise {@code false}.
 */
def isDeployableBranch(String branchName) {
  return ['develop', 'master'].contains(branchName) || branchName.startsWith('release/')
}

/**
 * Checks whether the current workspace contains a deployable branch.
 * Deployable branches are the develop, master and release branches as per Gitflow.
 *
 * @return @return {@code true} for deployable branches, otherwise {@code false}.
 */
def isDeployableBranch() {
  return isDeployableBranch(env.BRANCH_NAME)
}
