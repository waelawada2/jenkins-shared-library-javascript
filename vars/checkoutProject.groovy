#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*

/**
 * Checks out the project from Git.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def scmVars = checkout scm
  branchName = scmVars.GIT_BRANCH

  def du = new DeploymentUtils().createDeploymentUnit()

  echo "===============================\n" +
      "Checked out project:\n" +
      "Application: ${du.artifactId}\n" +
      "Version: ${du.version}\n" +
      "Group ID: ${du.groupId}\n" +
      "Git URL: ${scmVars.GIT_URL}\n" +
      "Branch: ${scmVars.GIT_BRANCH}\n" +
      "Commit: ${scmVars.GIT_COMMIT}\n" +
      "==============================="

}