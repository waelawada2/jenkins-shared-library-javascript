#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*
import static com.sothebys.jenkins.js.Constants.*

/**
 * Creates a Docker image with the build results.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  // parameters
  def application = config.application
  def tag = config.tag

  if (!application || !tag) {
    // create DeploymentUnit instance only if parameters were not provided
    def du = new DeploymentUtils().createDeploymentUnit()
    application = du.artifactId
    tag = du.dockerTag
  }

  // Unpack the distribution archive before building the Docker image
  sh "unzip ${DEFAULT_DIST_ARCHIVE_NAME} -d dist"

  sh "docker build -t ${application}:${tag} ."

  echo "===============================\n" +
      "Docker image created:\n" +
      "Docker image: ${application}\n" +
      "Docker tag: ${tag}\n" +
      "==============================="

}