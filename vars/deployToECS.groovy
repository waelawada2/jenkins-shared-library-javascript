#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*
import static com.sothebys.jenkins.js.Constants.*

/**
 * Deploys an application to an ECS cluster.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def du = new DeploymentUtils().createDeploymentUnit()

  // parameters
  def application = config.application ?: du.artifactId
  def registry = config.registry ?: "${ECR_BASE_URI}/${application}"
  def environment = config.environment ?: env.DEPLOYMENT_ENVIRONMENT
  def cluster = config.cluster ?: "stb-${environment.toLowerCase()}"
  def tag = config.tag ?: (env.DOCKER_TAG ?: du.dockerTag)
  def containerName = config.containerName ?: application
  def containerPort = config.containerPort

  if (!containerPort) {
    containerPort = sh script: "cat Dockerfile | grep 'EXPOSE' | tr -d -c 0-9", returnStdout: true
  }

  echo "-------------------------------\n" +
      "Deploy ${application}-${tag} to ${environment.toUpperCase()} environment\n" +
      "-------------------------------"

  withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: AWS_CREDENTIALS[environment.toLowerCase()], accessKeyVariable: 'AWS_ACCESS_KEY_ID', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
    withEnv(["APP_NAME=${application}", "REGISTRY=${registry}", "CLUSTER=${cluster}", "ENVIRONMENT=${environment}", "TAG=${tag}", "CONTAINER_PORT=${containerPort}", "CONTAINER_NAME=${containerName}"]) {
      sh '''
        SERVICE_NAME="${APP_NAME}-${ENVIRONMENT}"
        TASK_FAMILY="${APP_NAME}-${ENVIRONMENT}"
 
      # Create new task definition
      sed -e "s;%IMAGE_URI%;${REGISTRY}:${TAG};g" taskdef.${ENVIRONMENT}.ecs.json > taskdef-${TAG}.${ENVIRONMENT}.ecs.json
      aws ecs register-task-definition --family ${TASK_FAMILY} --cli-input-json file://taskdef-${TAG}.${ENVIRONMENT}.ecs.json
 
      # Create or update service
      SERVICE_EXISTS=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} | jq .failures[]`
      SERVICE_STATUS=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} | jq -r .services[0].status`
      if [ "$SERVICE_EXISTS" == "" ] && [ "${SERVICE_STATUS}" != "INACTIVE" ]; then
        echo "Updating existing service"
        DESIRED_COUNT=`aws ecs describe-services --services ${SERVICE_NAME} --cluster ${CLUSTER} | jq .services[].desiredCount`
        if [ ${DESIRED_COUNT} = "0" ]; then
          DESIRED_COUNT="1"
        fi
        TASK_REVISION=`aws ecs describe-task-definition --task-definition ${TASK_FAMILY} | jq .taskDefinition.revision`
        aws ecs update-service --service ${SERVICE_NAME} --cluster ${CLUSTER} --task-definition ${TASK_FAMILY}:${TASK_REVISION} --desired-count ${DESIRED_COUNT}
      else
        echo "Creating new service"
        TARGET_GROUP_ARN=`aws elbv2 describe-target-groups | jq -r ".TargetGroups[] | select(.TargetGroupName | contains(\\"${SERVICE_NAME}\\")) | .TargetGroupArn"`
        aws ecs create-service --service-name ${SERVICE_NAME} --cluster ${CLUSTER} --task-definition ${TASK_FAMILY} --desired-count 1 --load-balancers "targetGroupArn=${TARGET_GROUP_ARN},containerName=${CONTAINER_NAME},containerPort=${CONTAINER_PORT}" --role ecsServiceRole
      fi
    '''
    }
  }

  echo "===============================\n" +
      "Deployed to ECS:\n" +
      "Application: ${application}\n" +
      "Docker image: ${registry}:${tag}\n" +
      "Environment: ${environment}\n" +
      "Cluster: ${cluster}\n" +
      "==============================="

}
