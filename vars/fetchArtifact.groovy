#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*
import static com.sothebys.jenkins.js.Constants.*

/**
 * Fetches an artifact from the artifact repository.
 *
 * @author Gregor Zurowski
 */
def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def du = new DeploymentUtils().createDeploymentUnit()

  // parameters
  def environment = (config.environment ?: env.DEPLOYMENT_ENVIRONMENT).toLowerCase()
  def branchName = params.branchName ?: env.BRANCH_NAME
  def artifactFile = config.artifactFile ?: DEFAULT_DIST_ARCHIVE_NAME
  def groupId = config.groupId ?: du.groupId
  def artifactId = config.artifactId ?: du.artifactId
  def repositoryVersion = config.version ?: du.createRepositoryVersionFor(du.version, branchName)

  // fetch artifact
  configFileProvider([configFile(fileId: DEFAULT_MAVEN_SETTINGS_ID, variable: 'MAVEN_SETTINGS')]) {
    def mvnHome = tool 'maven-3'
    sh "${mvnHome}/bin/mvn dependency:copy -U -B -Dmdep.stripVersion=true --settings $MAVEN_SETTINGS -DoutputDirectory=. -Dartifact=${groupId}:${artifactId}:${repositoryVersion}:zip"
    sh "mv ${artifactId}.zip $artifactFile"

    // fetch taskdef.ENVIRONMENT.ecs.yml
    sh "${mvnHome}/bin/mvn dependency:copy -U -B -Dmdep.stripVersion=true --settings ${env.MAVEN_SETTINGS} -DoutputDirectory=. -Dartifact=${groupId}:${artifactId}:${repositoryVersion}:json:taskdef.${environment}.ecs "
    sh "mv *-taskdef.*.ecs.json taskdef.${environment}.ecs.json"

    // fetch Dockerfile
    sh "${mvnHome}/bin/mvn dependency:copy -U -B -Dmdep.stripVersion=true --settings ${env.MAVEN_SETTINGS} -DoutputDirectory=. -Dartifact=${groupId}:${artifactId}:${repositoryVersion}:Dockerfile:Dockerfile "
    sh "mv *.Dockerfile Dockerfile"
  }

  echo "===============================\n" +
      "Fetched artifact:\n" +
      "Repository coordinates: ${groupId}:${artifactId}:${repositoryVersion}\n" +
      "Branch name: ${branchName}\n" +
      "Environment: ${environment}\n" +
      "Main artifact: ${artifactFile}\n" +
      "==============================="

}
