#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*
import static com.sothebys.jenkins.js.Constants.*

/**
 * Triggers a deployment to the next higher-level deployment environment.<br/>
 * <b>Must be called outside of a <code>node</code> block.</b>
 *
 * @author Gregor Zurowski
 */
def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  // parameters
  final def currentEnvironment = config.currentEnvironment ?: env.DEPLOYMENT_ENVIRONMENT
  final def promotionSubmitters = config.promotionSubmitters ?: env.PROMOTION_USERS
  final def dockerTagName = config.dockerTag ?: (env.DOCKER_TAG ?: DeploymentUnit.getDockerTag(env.VERSION, currentBuild.number))
  final def timeOutInDays = config.timeOutInDays ?: 1

  def nextEnvironment = DEPLOYMENT_ENVIRONMENTS[DEPLOYMENT_ENVIRONMENTS.indexOf(currentEnvironment).next()]

  if (nextEnvironment) {
    stage("Promote to ${nextEnvironment.toUpperCase()}") {
      def promote = true;
      timeout(time: timeOutInDays, unit: 'DAYS') {
        try {
          input message: "Promote to ${nextEnvironment.toUpperCase()}?", ok: 'OK', submitter: promotionSubmitters
        } catch (org.jenkinsci.plugins.workflow.steps.FlowInterruptedException e) {
          echo "Promotion to ${nextEnvironment} cancelled."
          promote = false;
        }
      }
      if (promote) {
        node {
          triggerDeployment {
            environment = nextEnvironment
            dockerTag = dockerTagName
            withPromotion = 'true'
          }
        }
      }
    }
  }

  echo "===============================\n" +
       "Deployment promoted:\n" +
       "Environment: ${nextEnvironment} (current: ${currentEnvironment})\n" +
       "Docker Tag: ${dockerTagName}\n" +
       "Submitters: ${promotionSubmitters}\n" +
       "==============================="
}
