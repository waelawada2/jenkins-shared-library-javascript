#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*
import static com.sothebys.jenkins.js.Constants.*

/**
 * Publishes the build artifact in the artifact repositry.
 *
 * @author Gregor Zurowski
 */
def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  def du = new DeploymentUtils().createDeploymentUnit()

  // parameters
  def distributionDirectory = config.distributionDirectory ?: DEFAULT_DIST_DIR
  def artifactFile = config.artifactFile ?: DEFAULT_DIST_ARCHIVE_NAME

  def repositoryId = du.isSnapshot() ? REPO_ID_SOTHEBYS_SNAPSHOTS : REPO_ID_SOTHEBYS_RELEASES
  def repositoryUrl = REPO_URL_SOTHEBYS + repositoryId

  // create version that includes the branch name, if required
  def repositoryVersion = du.createRepositoryVersionFor(env.BRANCH_NAME)

  def pwd = sh script: 'pwd', returnStdout: true

  // create artifact archive and move to root
  sh "cd ${distributionDirectory} && zip -r ${artifactFile} * && mv ${artifactFile} ${pwd}"

  // publish artifact in Nexus

  // Deploy main artifact along with the Dockerfile and ECS task definitions.
  // See: http://maven.apache.org/plugins/maven-deploy-plugin/deploy-file-mojo.html
  // See: http://maven.apache.org/plugins/maven-deploy-plugin/examples/deploying-with-classifiers.html
  configFileProvider([configFile(fileId: DEFAULT_MAVEN_SETTINGS_ID, variable: 'MAVEN_SETTINGS')]) {
    def mvnHome = tool 'maven-3'
    sh "${mvnHome}/bin/mvn -B --settings ${MAVEN_SETTINGS} deploy:deploy-file -Dfile=${artifactFile} -Dfiles=Dockerfile,taskdef.dev.ecs.json,taskdef.prd.ecs.json,taskdef.qa.ecs.json,taskdef.stg.ecs.json -DrepositoryId=$repositoryId -Durl=$repositoryUrl -DgroupId=${du.groupId} -DartifactId=${du.artifactId} -Dversion=${repositoryVersion} -Dclassifiers=Dockerfile,taskdef.dev.ecs,taskdef.prd.ecs,taskdef.qa.ecs,taskdef.stg.ecs -Dtypes=Dockerfile,json,json,json,json"
  }

  // remove artifact archive after uploading to Nexus
  sh "rm $artifactFile"

  echo "===============================\n" +
      "Published artifact:\n" +
      "Repository coordinates: ${du.groupId}:${du.artifactId}:${repositoryVersion}\n" +
      "Repository URL: ${repositoryUrl}\n" +
      "Main artifact: ${artifactFile}\n" +
      "==============================="

}
