#!/usr/bin/env groovy
import com.sothebys.jenkins.js.*

/**
 * Publishes artifact version and docker tag on the build page of a build job or deployment job.
 */
def call(body) {
    def config = [:]
    body.resolveStrategy = Closure.DELEGATE_FIRST
    body.delegate = config
    body()

    def du = new DeploymentUtils().createDeploymentUnit()
    def providedVersion = env.VERSION
    def providedDockerTag = env.DOCKER_TAG
    def branchName = env.BRANCH_NAME
    def version = config.version ?: (du.isSnapshot() && branchName != 'develop' ? du.createVersionWithBranchName(branchName) : du.version)

    currentBuild.result = (!currentBuild.result || currentBuild.result == 'SUCCESS') ? "SUCCESS" : "FAILURE"

    if (providedVersion == null && providedDockerTag == null) {
        rtp parserName: "HTML", stableText: "<br/><br/><b>Artifact Id</b><br/> $version <br/>"
    } else if (providedVersion != null) {
        rtp parserName: "HTML", stableText: "<br/><br/><b>Deployed Artifact Version:</b><br/>$providedVersion<br/><b>Generated Docker Tag</b><br/>$du.dockerTag<br/>"
    } else if (providedDockerTag != null) {
        rtp parserName: "HTML", stableText: "<br/><br/><b>Deployed Docker tag</b><br/>$providedDockerTag"
    }
}