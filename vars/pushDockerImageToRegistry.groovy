#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*
import static com.sothebys.jenkins.js.Constants.*

/**
 * Pushes the Docker image to a Docker registry.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  // parameters
  def application = config.application
  def tag = config.tag
  def registry = config.registry

  if (!application || !tag) {
    // create DeploymentUnit instance only if parameters were not provided
    def du = new DeploymentUtils().createDeploymentUnit()
    application = du.artifactId
    tag = du.dockerTag
  }

  if (!registry) {
    registry = "${ECR_BASE_URI}/${application}"
  }

  withCredentials([[$class: 'AmazonWebServicesCredentialsBinding', credentialsId: AWS_CREDENTIALS['prd'], accessKeyVariable: 'AWS_ACCESS_KEY_ID', secretKeyVariable: 'AWS_SECRET_ACCESS_KEY']]) {
    withEnv(["APPLICATION=${application}", "URI=${registry}", "TAG=${tag}"]) {
      sh '''
       docker tag ${APPLICATION}:${TAG} ${URI}:${TAG}
       $(aws ecr get-login --region us-east-1 --no-include-email)
       docker push ${URI}:${TAG}
       '''
    }
  }

  echo "===============================\n" +
       "Pushed Docker image:\n" +
       "Source image: ${application}:${tag}\n" +
       "Pushed image: ${registry}:${tag}\n" +
       "==============================="
}
