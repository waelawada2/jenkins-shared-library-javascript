#!/usr/bin/env groovy

/**
 * Send a build notifications via e-mail.
 *
 * @author Gregor Zurowski
 */
def call(body) {

  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  // parameters
  final def recipients = config.recipients ?: ''

  def success = !currentBuild.result || currentBuild.result == 'SUCCESS'
  def branch = env.BRANCH_NAME

  // email
  def subject = "${success ? 'SUCCESS' : 'FAILED'}: ${URLDecoder.decode(env.JOB_NAME, 'UTF-8')} - Build #${env.BUILD_NUMBER}"
  def emailBody = """Job '${URLDecoder.decode(env.JOB_NAME, 'UTF-8')}' #${env.BUILD_NUMBER}${
    branch != null ? ' (branch \'' + branch + '\') ' : ' '
  }${success ? 'SUCCESS' : 'FAILED'}:

- Summary available at ${env.BUILD_URL}

- Console output available at ${env.BUILD_URL}console"""

  emailext recipients: recipients,
      recipientProviders: [[$class: 'CulpritsRecipientProvider'], [$class: 'RequesterRecipientProvider']],
      subject: subject,
      body: emailBody

}
