#!/usr/bin/env groovy

import com.sothebys.jenkins.js.*

/**
 * Triggers a deployment job in Jenkins.
 *
 * @param environment The target deployment environment.
 * @param dockerTag The Docker image tag. Optional parameter.
 * @param withPromotion Whether the deployment job starts a promotion chain to higher environments. Optional parameter.
 *
 * @author Gregor Zurowski
 */
def call(body) {
  def config = [:]
  body.resolveStrategy = Closure.DELEGATE_FIRST
  body.delegate = config
  body()

  // parameters
  def environment = config.environment
  def dockerTag = config.dockerTag
  def withPromotion = config.withPromotion

  def du = new DeploymentUtils().createDeploymentUnit()
  def artifactId = du.artifactId
  // create version that includes the branch name, if required
  def version = du.createRepositoryVersionFor(env.BRANCH_NAME)

  // Try to determine where the deployment job is located:
  // - one folder level up, if invoked from a multibranch pipeline -> '..'
  // - parallel to the current job if invoked from another deployment job -> '.'
  def jobPathPrefix = env.BRANCH_NAME && env.JOB_NAME.endsWith(URLEncoder.encode(env.BRANCH_NAME, 'UTF-8')) ? '..' : '.'

  build job: "${jobPathPrefix}/${artifactId}-deployment-${environment}",
      parameters: [
          string(name: 'BRANCH_NAME', value: env.BRANCH_NAME),
          string(name: 'DOCKER_TAG', value: dockerTag ?: ''),
          string(name: 'VERSION', value: version ?: ''),
          string(name: 'WITH_PROMOTION', value: withPromotion ?: ''),
      ],
      wait: false

  echo "===============================\n" +
      "Triggered deployment job '${jobPathPrefix}/${artifactId}-deployment-${environment}' with parameters:\n" +
      "Branch name: ${env.BRANCH_NAME}\n" +
      "Docker tag: ${dockerTag ?: ''}\n" +
      "Version: ${version}\n" +
      "With promotion: ${withPromotion ?: ''}\n" +
      "==============================="
}
