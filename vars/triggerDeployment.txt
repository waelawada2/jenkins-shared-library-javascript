Pushes the Docker image to a Docker registry.
<br>
<b>Mandatory parameter: </b>environment<br>
<p style="text-indent: 40px">The target environment where the application should be deployed to (e.g. <code>dev</code>).</p>
<b>Optional parameter: </b>tag<br>
<p style="text-indent: 40px">The Docker tag of the image that should be deployed.</p>
<b>Optional parameter: </b>withPromotion<br>
<p style="text-indent: 40px">If deployment promotion should be used, then this parameter must be set to <code>true</code>.</p>
